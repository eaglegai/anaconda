From 92d8d9d3e39eae8e268e3aeff096105b441bbeae Mon Sep 17 00:00:00 2001
From: Jiri Konecny <jkonecny@redhat.com>
Date: Mon, 22 Jun 2020 13:12:53 +0200
Subject: [PATCH] Rename function for a simple check for DNF repository

It's more clear to use repository in the name than installtree.

Related: rhbz#1844287
Related: rhbz#1849093
---
 .../payloads/source/harddrive/initialization.py    |  4 ++--
 .../modules/payloads/source/nfs/initialization.py  |  4 ++--
 pyanaconda/modules/payloads/source/utils.py        |  6 +++---
 .../pyanaconda_tests/module_source_base_test.py    | 14 +++++++-------
 .../module_source_harddrive_test.py                | 12 ++++++------
 .../pyanaconda_tests/module_source_nfs_test.py     | 12 ++++++------
 6 files changed, 26 insertions(+), 26 deletions(-)

diff --git a/pyanaconda/modules/payloads/source/harddrive/initialization.py b/pyanaconda/modules/payloads/source/harddrive/initialization.py
index ed77db6bc9..004df4f034 100644
--- a/pyanaconda/modules/payloads/source/harddrive/initialization.py
+++ b/pyanaconda/modules/payloads/source/harddrive/initialization.py
@@ -22,7 +22,7 @@
 from pyanaconda.modules.common.errors.payload import SourceSetupError
 from pyanaconda.modules.common.task import Task
 from pyanaconda.modules.payloads.source.utils import find_and_mount_device, \
-    find_and_mount_iso_image, verify_valid_installtree
+    find_and_mount_iso_image, verify_valid_repository
 from pyanaconda.payload.utils import unmount
 from pyanaconda.anaconda_loggers import get_module_logger
 
@@ -82,7 +82,7 @@ def run(self):
             log.debug("Using the ISO '%s' mounted at '%s'.", iso_name, self._iso_mount)
             return SetupHardDriveResult(self._iso_mount, iso_name)
 
-        if verify_valid_installtree(full_path_on_mounted_device):
+        if verify_valid_repository(full_path_on_mounted_device):
             log.debug("Using the directory at '%s'.", full_path_on_mounted_device)
             return SetupHardDriveResult(full_path_on_mounted_device, "")
 
diff --git a/pyanaconda/modules/payloads/source/nfs/initialization.py b/pyanaconda/modules/payloads/source/nfs/initialization.py
index 56e95060c6..99601bf325 100644
--- a/pyanaconda/modules/payloads/source/nfs/initialization.py
+++ b/pyanaconda/modules/payloads/source/nfs/initialization.py
@@ -22,7 +22,7 @@
 from pyanaconda.modules.common.errors.payload import SourceSetupError
 from pyanaconda.modules.common.task import Task
 from pyanaconda.modules.payloads.source.utils import find_and_mount_iso_image, \
-    verify_valid_installtree
+    verify_valid_repository
 from pyanaconda.payload.errors import PayloadSetupError
 from pyanaconda.payload.utils import mount, unmount
 
@@ -65,7 +65,7 @@ def run(self):
             log.debug("Using the ISO '%s' mounted at '%s'.", iso_name, self._iso_mount)
             return self._iso_mount
 
-        if verify_valid_installtree(self._device_mount):
+        if verify_valid_repository(self._device_mount):
             log.debug("Using the directory at '%s'.", self._device_mount)
             return self._device_mount
 
diff --git a/pyanaconda/modules/payloads/source/utils.py b/pyanaconda/modules/payloads/source/utils.py
index ed9e5da49b..84cdd33ca8 100644
--- a/pyanaconda/modules/payloads/source/utils.py
+++ b/pyanaconda/modules/payloads/source/utils.py
@@ -148,10 +148,10 @@ def _create_iso_path(path, iso_name):
     return path
 
 
-def verify_valid_installtree(path):
-    """Check if the given path is a valid installtree repository.
+def verify_valid_repository(path):
+    """Check if the given path is a valid repository.
 
-    :param str path: install tree path
+    :param str path: path to the repository
     :returns: True if repository is valid false otherwise
     :rtype: bool
     """
diff --git a/tests/nosetests/pyanaconda_tests/module_source_base_test.py b/tests/nosetests/pyanaconda_tests/module_source_base_test.py
index 3ba40edf4c..c9f00fa4f5 100644
--- a/tests/nosetests/pyanaconda_tests/module_source_base_test.py
+++ b/tests/nosetests/pyanaconda_tests/module_source_base_test.py
@@ -26,7 +26,7 @@
 from pyanaconda.modules.payloads.source.mount_tasks import SetUpMountTask, TearDownMountTask
 from pyanaconda.modules.payloads.source.source_base import MountingSourceMixin
 from pyanaconda.modules.payloads.source.utils import find_and_mount_iso_image, \
-    verify_valid_installtree
+    verify_valid_repository
 
 mount_location = "/some/dir"
 
@@ -188,20 +188,20 @@ def find_and_mount_iso_image_fail_mount_test(self,
 
         self.assertEqual(iso_name, "")
 
-    def verify_valid_installtree_success_test(self):
-        """Test verify_valid_installtree functionality success."""
+    def verify_valid_repository_success_test(self):
+        """Test verify_valid_repository functionality success."""
         with TemporaryDirectory() as tmp:
             repodir_path = Path(tmp, "repodata")
             repodir_path.mkdir()
             repomd_path = Path(repodir_path, "repomd.xml")
             repomd_path.write_text("This is a cool repomd file!")
 
-            self.assertTrue(verify_valid_installtree(tmp))
+            self.assertTrue(verify_valid_repository(tmp))
 
-    def verify_valid_installtree_failed_test(self):
-        """Test verify_valid_installtree functionality failed."""
+    def verify_valid_repository_failed_test(self):
+        """Test verify_valid_repository functionality failed."""
         with TemporaryDirectory() as tmp:
             repodir_path = Path(tmp, "repodata")
             repodir_path.mkdir()
 
-            self.assertFalse(verify_valid_installtree(tmp))
+            self.assertFalse(verify_valid_repository(tmp))
diff --git a/tests/nosetests/pyanaconda_tests/module_source_harddrive_test.py b/tests/nosetests/pyanaconda_tests/module_source_harddrive_test.py
index dff84b6d19..99be32fa1f 100644
--- a/tests/nosetests/pyanaconda_tests/module_source_harddrive_test.py
+++ b/tests/nosetests/pyanaconda_tests/module_source_harddrive_test.py
@@ -211,10 +211,10 @@ def success_find_iso_test(self,
            return_value=True)
     @patch("pyanaconda.modules.payloads.source.harddrive.initialization.find_and_mount_iso_image",
            return_value="")
-    @patch("pyanaconda.modules.payloads.source.harddrive.initialization.verify_valid_installtree",
+    @patch("pyanaconda.modules.payloads.source.harddrive.initialization.verify_valid_repository",
            return_value=True)
     def success_find_dir_test(self,
-                              verify_valid_installtree_mock,
+                              verify_valid_repository_mock,
                               find_and_mount_iso_image_mock,
                               find_and_mount_device_mock):
         """Hard drive source setup dir found."""
@@ -228,7 +228,7 @@ def success_find_dir_test(self,
         find_and_mount_iso_image_mock.assert_called_once_with(
             device_mount_location + path_on_device, iso_mount_location
         )
-        verify_valid_installtree_mock.assert_called_once_with(
+        verify_valid_repository_mock.assert_called_once_with(
             device_mount_location + path_on_device
         )
         self.assertEqual(result, SetupHardDriveResult(device_mount_location + path_on_device, ""))
@@ -237,12 +237,12 @@ def success_find_dir_test(self,
            return_value=True)
     @patch("pyanaconda.modules.payloads.source.harddrive.initialization.find_and_mount_iso_image",
            return_value="")
-    @patch("pyanaconda.modules.payloads.source.harddrive.initialization.verify_valid_installtree",
+    @patch("pyanaconda.modules.payloads.source.harddrive.initialization.verify_valid_repository",
            return_value=False)
     @patch("pyanaconda.modules.payloads.source.harddrive.initialization.unmount")
     def failure_to_find_anything_test(self,
                                       unmount_mock,
-                                      verify_valid_installtree_mock,
+                                      verify_valid_repository_mock,
                                       find_and_mount_iso_image_mock,
                                       find_and_mount_device_mock):
         """Hard drive source setup failure to find anything."""
@@ -257,7 +257,7 @@ def failure_to_find_anything_test(self,
         find_and_mount_iso_image_mock.assert_called_once_with(
             device_mount_location + path_on_device, iso_mount_location
         )
-        verify_valid_installtree_mock.assert_called_once_with(
+        verify_valid_repository_mock.assert_called_once_with(
             device_mount_location + path_on_device
         )
         unmount_mock.assert_called_once_with(
diff --git a/tests/nosetests/pyanaconda_tests/module_source_nfs_test.py b/tests/nosetests/pyanaconda_tests/module_source_nfs_test.py
index eb331dec10..d33796a469 100644
--- a/tests/nosetests/pyanaconda_tests/module_source_nfs_test.py
+++ b/tests/nosetests/pyanaconda_tests/module_source_nfs_test.py
@@ -180,7 +180,7 @@ def success_find_iso_test(self,
 
         self.assertEqual(result, iso_mount_location)
 
-    @patch("pyanaconda.modules.payloads.source.nfs.initialization.verify_valid_installtree",
+    @patch("pyanaconda.modules.payloads.source.nfs.initialization.verify_valid_repository",
            return_value=True)
     @patch("pyanaconda.modules.payloads.source.nfs.initialization.find_and_mount_iso_image",
            return_value="")
@@ -188,7 +188,7 @@ def success_find_iso_test(self,
     def success_find_dir_test(self,
                               mount_mock,
                               find_and_mount_iso_image_mock,
-                              verify_valid_installtree_mock):
+                              verify_valid_repository_mock):
         """Test NFS source setup find installation tree success"""
         task = _create_setup_task()
         result = task.run()
@@ -201,7 +201,7 @@ def success_find_dir_test(self,
         find_and_mount_iso_image_mock.assert_called_once_with(device_mount_location,
                                                               iso_mount_location)
 
-        verify_valid_installtree_mock.assert_called_once_with(device_mount_location)
+        verify_valid_repository_mock.assert_called_once_with(device_mount_location)
 
         self.assertEqual(result, device_mount_location)
 
@@ -252,7 +252,7 @@ def setup_install_source_task_mount_failure_test(self, mount_mock):
                                            options="nolock")
 
     @patch("pyanaconda.modules.payloads.source.nfs.initialization.unmount")
-    @patch("pyanaconda.modules.payloads.source.nfs.initialization.verify_valid_installtree",
+    @patch("pyanaconda.modules.payloads.source.nfs.initialization.verify_valid_repository",
            return_value=False)
     @patch("pyanaconda.modules.payloads.source.nfs.initialization.find_and_mount_iso_image",
            return_value="")
@@ -260,7 +260,7 @@ def setup_install_source_task_mount_failure_test(self, mount_mock):
     def setup_install_source_task_find_anything_failure_test(self,
                                                              mount_mock,
                                                              find_and_mount_iso_image_mock,
-                                                             verify_valid_installtree_mock,
+                                                             verify_valid_repository_mock,
                                                              unmount_mock):
         """Test NFS can't find anything to install from"""
         task = SetUpNFSSourceTask(device_mount_location, iso_mount_location, nfs_url)
@@ -274,7 +274,7 @@ def setup_install_source_task_find_anything_failure_test(self,
         find_and_mount_iso_image_mock.assert_called_once_with(device_mount_location,
                                                               iso_mount_location)
 
-        verify_valid_installtree_mock.assert_called_once_with(device_mount_location)
+        verify_valid_repository_mock.assert_called_once_with(device_mount_location)
 
         unmount_mock.assert_called_once_with(
             device_mount_location
