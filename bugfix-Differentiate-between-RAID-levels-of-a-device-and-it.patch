From c9857a91ece047c0fc2df3554e625d15b4700818 Mon Sep 17 00:00:00 2001
From: Vendula Poncova <vponcova@redhat.com>
Date: Thu, 13 Aug 2020 13:04:14 +0200
Subject: [PATCH] Differentiate between RAID levels of a device and its
 container

The current logic returned the same RAID level for the device and its container,
but we expect that only one of them will have the RAID level set.
---
 .../partitioning/interactive/add_device.py    |  4 +-
 .../storage/partitioning/interactive/utils.py | 37 +++++++++++++++----
 2 files changed, 31 insertions(+), 10 deletions(-)

diff --git a/pyanaconda/modules/storage/partitioning/interactive/add_device.py b/pyanaconda/modules/storage/partitioning/interactive/add_device.py
index 82bb9917a..852cf8fbd 100644
--- a/pyanaconda/modules/storage/partitioning/interactive/add_device.py
+++ b/pyanaconda/modules/storage/partitioning/interactive/add_device.py
@@ -24,7 +24,7 @@ from pyanaconda.modules.common.errors.configuration import StorageConfigurationE
 from pyanaconda.modules.common.structures.device_factory import DeviceFactoryRequest
 from pyanaconda.modules.common.task import Task
 from pyanaconda.modules.storage.partitioning.interactive.utils import \
-    get_device_raid_level_name, get_container_size_policy, get_device_factory_arguments
+    get_container_raid_level_name, get_container_size_policy, get_device_factory_arguments
 from pyanaconda.core.storage import PARTITION_ONLY_FORMAT_TYPES
 
 log = get_module_logger(__name__)
@@ -141,7 +141,7 @@ class AddDeviceTask(Task):
             # Don't override user-initiated changes to a defined container.
             request.disks = [d.name for d in container.disks]
             request.container_encrypted = container.encrypted
-            request.container_raid_level = get_device_raid_level_name(container)
+            request.container_raid_level = get_container_raid_level_name(container)
             request.container_size_policy = get_container_size_policy(container)
 
             # The existing container has a name.
diff --git a/pyanaconda/modules/storage/partitioning/interactive/utils.py b/pyanaconda/modules/storage/partitioning/interactive/utils.py
index 04313eded..d3e56030a 100644
--- a/pyanaconda/modules/storage/partitioning/interactive/utils.py
+++ b/pyanaconda/modules/storage/partitioning/interactive/utils.py
@@ -696,12 +696,6 @@ def get_device_raid_level(device):
     if hasattr(device, "data_level"):
         return device.data_level
 
-    if hasattr(device, "volume"):
-        return device.volume.data_level
-
-    if not hasattr(device, "vg") and hasattr(device, "lvs") and len(device.parents) == 1:
-        return get_device_raid_level(device.parents[0])
-
     return None
 
 
@@ -711,6 +705,33 @@ def get_device_raid_level_name(device):
     return raid_level.name if raid_level else ""
 
 
+def get_container_raid_level(container):
+    """Get the RAID level of the given container.
+
+    :param container: a container
+    :return: a RAID level
+    """
+    # Try to get a RAID level of this device.
+    raid_level = get_device_raid_level(container)
+
+    if raid_level:
+        return raid_level
+
+    device = container.raw_device
+
+    # Or get a RAID level of the LVM container.
+    if hasattr(device, "lvs") and len(device.parents) == 1:
+        return get_container_raid_level(device.parents[0])
+
+    return None
+
+
+def get_container_raid_level_name(device):
+    """Get the RAID level name of the given container."""
+    raid_level = get_container_raid_level(device)
+    return raid_level.name if raid_level else ""
+
+
 def collect_file_system_types(device):
     """Collect supported file system types for the given device.
 
@@ -855,7 +876,7 @@ def set_container_data(request: DeviceFactoryRequest, container):
     request.container_spec = container.name
     request.container_name = container.name
     request.container_encrypted = container.encrypted
-    request.container_raid_level = get_device_raid_level_name(container)
+    request.container_raid_level = get_container_raid_level_name(container)
     request.container_size_policy = get_container_size_policy(container)
 
     if request.container_encrypted:
@@ -1100,7 +1121,7 @@ def _destroy_device(storage, device):
             disks=container.disks,
             container_name=container.name,
             container_encrypted=container.encrypted,
-            container_raid_level=get_device_raid_level(container),
+            container_raid_level=get_container_raid_level(container),
             container_size=container.size_policy,
         )
 
-- 
2.23.0

